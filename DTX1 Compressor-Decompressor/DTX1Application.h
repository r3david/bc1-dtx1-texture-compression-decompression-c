//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef RL_DTX1_APP
#define RL_DTX1_APP

#include "Image.h"
#include "BMPImage.h"
#include "DDSImage.h"
#include "BC1Compression.h"

namespace DTX1CompressorDecompressor
{
	class DTX1Application
	{
	public:		
		void Run();

	private:
		void OpenImage(Image* img);
		void SaveImage(Image* img);
		void BmpLoadingState();
		void DdsLoadingState();
		void Deinitialize();

		BMPImage m_loadedBMPImg;
		BMPImage m_decompressedBMPImg;
		BC1DDSImage m_loadedDDSImg;
		BC1DDSImage m_compressedDDSImg;
		BC1Compression m_compression;
	};

}

#endif // RL_DTX1_APP