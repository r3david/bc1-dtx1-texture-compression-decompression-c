Copyright (c) 2017 Ricardo David CM - http://ricardo-david.com  

### CONTENT 

This repository contains an implementation of DTX1 - BC1 texture compression and decompression using C++. This sample implementation only works for bitmap .bmp images of 24bit color.

The code has been put in an easy to read format rather than being the most performant algorithm. It implements the approach described in here:

https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx

The code contains a VS2015 project and it doesn't use any external library. 

-------------------------------------------